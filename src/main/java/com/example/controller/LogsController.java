package com.example.controller;

import com.example.entity.Deploy;
import com.example.repository.DeployRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by user on 21/5/16.
 */
@Controller
public class LogsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogsController.class);

    @Value("${logs.url}")
    private String url;
    @Autowired
    DeployRepository deployRepository;

    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @RequestMapping("/index")
    public String index( ModelMap modelMap,String env) {
        List<String> envList = deployRepository.findEnvList();
        modelMap.put("envList",envList);
        if(!StringUtils.isEmpty(env)){
            modelMap.put("env",env);
        }else if(!CollectionUtils.isEmpty(envList)){
            modelMap.put("env",envList.get(0));
        }
        return "index";
    }
    @RequestMapping("/findList")
    public String list(@RequestParam String env, String name, ModelMap modelMap) {
        if(null!=name && !StringUtils.isEmpty(name.trim())){
            List<Deploy> deployList = deployRepository.findByEnvAndNameLike(env,"%"+name+"%");
            modelMap.put("deployList",deployList);
        }else {
            List<Deploy> deployList = deployRepository.findByEnv(env);
            modelMap.put("deployList",deployList);
        }
        return "list";
    }

    @RequestMapping("/logs/{uuid}")
    public String showLogs(@PathVariable("uuid") String uuid , ModelMap modelMap, HttpServletRequest request) {
        String bash = request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        if(StringUtils.isEmpty(url)){
            modelMap.put("wsUrl","ws://"+bash+"/ws/logs/"+uuid);
        }else {
            modelMap.put("wsUrl","ws://"+url+"/ws/logs/"+uuid);
        }
        modelMap.put("uuid",uuid);
        //modelMap.put("wsUrl","ws://"+bash+"/ws/logs/"+uuid);
        return "logs";
    }

    @RequestMapping(value ="/config/add", method = RequestMethod.GET)
    public String toSave() {
        return "add";
    }

    @RequestMapping(value = "/config/add",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> save(Deploy deploy) {
        Map<String,Object> map=new HashMap<String, Object>();
        deploy.setUuid(getUUId());
        Deploy save = deployRepository.save(deploy);
        map.put("success",true);
        map.put("data",save);
        return map;
    }
    @RequestMapping(value = "/logs/del/{uuid}")
    public String del(@PathVariable("uuid") String uuid, String env, HttpServletRequest request) {
       // request.getHeader("Referer");
        deployRepository.delete(uuid);
        return "redirect:/index?env="+env;
    }

    private  String getUUId() {
        int first = new Random(10).nextInt(8) + 1;
        System.out.println(first);
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        if (hashCodeV < 0) {//有可能是负数
            hashCodeV = -hashCodeV;
        }
        // 0 代表前面补充0
        // 4 代表长度为4
        // d 代表参数为正数型
        return first + String.format("%015d", hashCodeV);
    }
}
