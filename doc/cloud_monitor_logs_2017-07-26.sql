# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 192.168.1.96 (MySQL 5.6.20-log)
# Database: cloud_monitor_logs
# Generation Time: 2017-07-26 02:52:51 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table deploy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `deploy`;

CREATE TABLE `deploy` (
  `uuid` varchar(36) NOT NULL COMMENT 'UUID',
  `name` varchar(255) NOT NULL COMMENT '项目名称',
  `env` varchar(255) NOT NULL DEFAULT 'Dev' COMMENT '模块名称 枚举:Dev Test Aliyun',
  `user` varchar(255) DEFAULT 'root' COMMENT '服务器日志用户 ',
  `host` varchar(255) DEFAULT '' COMMENT '服务器 IP like:192.169.1.18',
  `ssh_port` int(11) NOT NULL COMMENT 'ssh 端口号',
  `log_path` varchar(255) DEFAULT '' COMMENT '日志路径',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `deploy` WRITE;
/*!40000 ALTER TABLE `deploy` DISABLE KEYS */;

INSERT INTO `deploy` (`uuid`, `name`, `env`, `user`, `host`, `ssh_port`, `log_path`)
VALUES
	('1000000','cms-web','Dev','root','192.168.1.18',22,'/data/app/web/cms-web/logs/stdout.log'),
	('1000001','customer-web','Dev','root','192.168.1.18',22,'/data/app/web/customer-web/logs/stdout.log'),
	('1000002','getway','Dev','root','192.168.1.18',22,'/data/app/web/getway/logs/stdout.log'),
	('1000003','weixin','Dev','root','192.168.1.18',22,'/data/app/web/weixin/logs/stdout.log'),
	('2000000','cms-web','Test','root','192.168.1.85',22,'/data/app/web/cms-web/logs/stdout.log'),
	('2000001','customer-web','Test','root','192.168.1.85',22,'/data/app/web/customer-web/logs/stdout.log'),
	('2000002','getway','Test','root','192.168.1.85',22,'/data/app/web/getway/logs/stdout.log'),
	('2000003','weixin','Test','root','192.168.1.85',22,'/data/app/web/weixin/logs/stdout.log'),
	('3000000','actioncenter','Dev','root','192.168.1.18',22,'/data/app/services/actioncenter/logs/stdout.log'),
	('3000001','cart','Dev','root','192.168.1.18',22,'/data/app/services/cart/logs/stdout.log'),
	('3000002','cms','Dev','root','192.168.1.18',22,'/data/app/services/cms/logs/stdout.log'),
	('3000003','itemcenter','Dev','root','192.168.1.18',22,'/data/app/services/itemcenter/logs/stdout.log'),
	('3000004','resourcecenter','Dev','root','192.168.1.18',22,'/data/app/services/resourcecenter/logs/stdout.log'),
	('3000005','tradecenter','Dev','root','192.168.1.18',22,'/data/app/services/tradecenter/logs/stdout.log'),
	('3000006','ucenter','Dev','root','192.168.1.18',22,'/data/app/services/ucenter/logs/stdout.log'),
	('4000000','actioncenter','Test','root','192.168.1.85',22,'/data/app/services/actioncenter/logs/stdout.log'),
	('4000001','cart','Test','root','192.168.1.85',22,'/data/app/services/cart/logs/stdout.log'),
	('4000002','cms','Test','root','192.168.1.85',22,'/data/app/services/cms/logs/stdout.log'),
	('4000003','itemcenter','Test','root','192.168.1.85',22,'/data/app/services/itemcenter/logs/stdout.log'),
	('4000004','resourcecenter','Test','root','192.168.1.85',22,'/data/app/services/resourcecenter/logs/stdout.log'),
	('4000005','tradecenter','Test','root','192.168.1.85',22,'/data/app/services/tradecenter/logs/stdout.log'),
	('4000006','ucenter','Test','root','192.168.1.85',22,'/data/app/services/ucenter/logs/stdout.log');

/*!40000 ALTER TABLE `deploy` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
